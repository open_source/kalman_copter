
//#define DEBUG_TEST  

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.Globalization;
using System.Text;
using System.IO;


public class KalmanFilter : MonoBehaviour
{
    public Rigidbody imuBody;
    public Rigidbody gnssBody;
    public GameObject gnssMarker;
    public GameObject estimateMarker;
    public GameObject accMarker;
    public GameObject gyoMarker;

    public float SDNoiseGNSS;
    public Vector3 gyoBias, accBias, pos, vel;
    public Quaternion quat;
    public Vector3 sigma_gyoBias, sigma_accBias, sigma_pos, sigma_vel, sigma_euler;

    //size of vector state
    const int numStates = 16;

    //indexs of vector state
    const int numStateVelX = 0;
    const int numStateVelY = 1;
    const int numStateVelZ = 2;

    const int numStatePosX = 3;
    const int numStatePosY = 4;
    const int numStatePosZ = 5;

    const int numStateQ0 = 6;
    const int numStateQ1 = 7;
    const int numStateQ2 = 8;
    const int numStateQ3 = 9;

    const int numStateAccBiasX = 10;
    const int numStateAccBiasY = 11;
    const int numStateAccBiasZ = 12;

    const int numStateGyoBiasX = 13;
    const int numStateGyoBiasY = 14;
    const int numStateGyoBiasZ = 15;



    //initial values of states uncertainty (big values = big uncertainty, small = acurate value)
    const float DispVel = +3.2545e-02f;
    const float DispPos = +7.1562e+03f;
    const float DispAccBias = +1.3317e-03f;
    const float DispGyoBias = +4.5256e-02f;
    const float DispQuat = +5.4005e-04f;
    //values of dispesrion process noises (big value = strong correction from GNSS, small value = great confidence in the forecast)
    const float DispNoiseAccBias = +7.8673e-07f;
    const float DispNoiseGyoBias = +4.0297e-07f;
    const float DispNoiseRate = +1.7538e-05f;
    const float DispNoiseVel = +6.8304e-07f;
    /*************************************************************/

    private IntPtr ukf;
    private IMUModel imu;
    private GNSSModel gnss;
    private Vector3 localPosGPSOfIMU;
    private bool flagGnssReady = false;
    private Vector3 gnssData;

#if DEBUG_TEST
    private StreamWriter writer;
    private FileStream fs;
    private const char delimeter = ';';
#endif

    // Start is called before the first frame update
    private void OnEnable()
    {
        float[] States = new float[numStates];
        States[numStatePosX] = imuBody.position.x;
        States[numStatePosY] = imuBody.position.y;
        States[numStatePosZ] = imuBody.position.z;

        States[numStateVelX] = 0;
        States[numStateVelY] = 0;
        States[numStateVelZ] = 0;

        States[numStateGyoBiasX] = 0;
        States[numStateGyoBiasY] = 0;
        States[numStateGyoBiasZ] = 0;

        States[numStateAccBiasX] = 0;
        States[numStateAccBiasY] = 0;
        States[numStateAccBiasZ] = 0;

        States[numStateQ0] = imuBody.rotation.w;
        States[numStateQ1] = imuBody.rotation.x;
        States[numStateQ2] = imuBody.rotation.y;
        States[numStateQ3] = imuBody.rotation.z;

        ukf = KF_API.initKF(States);        
        KF_API.setInitVariance(ukf, DispVel, DispPos, DispAccBias, DispGyoBias, DispQuat);
        KF_API.setProcessVariance(ukf, DispNoiseVel, DispNoiseRate, DispNoiseAccBias, DispNoiseGyoBias);     



        //vector of local position of GNSS from IMU
        localPosGPSOfIMU = Quaternion.Inverse(imuBody.rotation) * (gnssBody.position - imuBody.position);

        imu = imuBody.GetComponent<IMUModel>();
        gnss = gnssBody.GetComponent<GNSSModel>();

        gnss.OnDataReady += gnss_ready;

#if DEBUG_TEST
        fs = new FileStream("logs.csv", FileMode.Create);
        writer = new StreamWriter(fs, Encoding.UTF8);

        string data = "time" + delimeter +
            "Rpx" + delimeter + "px" + delimeter + "sigma_px" + delimeter +
            "Rpy" + delimeter + "py" + delimeter + "sigma_py" + delimeter +
            "Rpz" + delimeter + "pz" + delimeter + "sigma_pz" + delimeter +

            "Rvx" + delimeter + "vx" + delimeter + "sigma_vx" + delimeter +
            "Rvy" + delimeter + "vy" + delimeter + "sigma_vy" + delimeter +
            "Rvz" + delimeter + "vz" + delimeter + "sigma_vz" + delimeter +

            "Rax" + delimeter + "ax" + delimeter + "sigma_ax" + delimeter +
            "Ray" + delimeter + "ay" + delimeter + "sigma_ay" + delimeter +
            "Raz" + delimeter + "az" + delimeter + "sigma_az" + delimeter +

            "Rgx" + delimeter + "gx" + delimeter + "sigma_gx" + delimeter +
            "Rgy" + delimeter + "gy" + delimeter + "sigma_gy" + delimeter +
            "Rgz" + delimeter + "gz" + delimeter + "sigma_gz" + delimeter +

            "Rex" + delimeter + "ex" + delimeter + "sigma_ex" + delimeter +
            "Rey" + delimeter + "ey" + delimeter + "sigma_ey" + delimeter +
            "Rez" + delimeter + "ez" + delimeter + "sigma_ez";
        writer.Write(data);
#endif
    }
    private void OnDisable()
    {
        KF_API.destroy(ukf);

#if DEBUG_TEST
        if (writer != null)
            writer.Dispose();
        if (fs != null)
            fs.Dispose();
#endif
    }
    public void SetSDGNSSNoise(string valStr)
    {        
        SDNoiseGNSS = float.Parse(valStr.Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator),  CultureInfo.InvariantCulture);
    }
    private Quaternion floatToQuaternion(float[] q)
    {
        Quaternion qq;
        qq.w = q[0];
        qq.x = q[1];
        qq.y = q[2];
        qq.z = q[3];
        return qq;
    }
    private float[] QuaternionTofloat(Quaternion qq)
    {
        float[] q = new float[4];
        q[0] = qq.w;
        q[1] = qq.x;
        q[2] = qq.y;
        q[3] = qq.z;
        return q;
    }
    private void gnss_ready(Vector3 data)
    {
        gnssData = data;
        flagGnssReady = true;
        if (gnssMarker != null)
        {
            gnssMarker.transform.position = data;
            gnssMarker.transform.rotation = gnss.transform.rotation;
        }
          
    }
    private void fromUKFtoVectors(IntPtr ukf, out Vector3 pos, out Vector3 vel, out Vector3 accBias, out Vector3 gyoBias, out Quaternion quat)
    {
        float[] State = new float[numStates];
        IntPtr pState = KF_API.getPtrState(ukf);
        Marshal.Copy(pState, State, 0, numStates);
        pos.x = State[numStatePosX];
        pos.y = State[numStatePosY];
        pos.z = State[numStatePosZ];
        vel.x = State[numStateVelX];
        vel.y = State[numStateVelY];
        vel.z = State[numStateVelZ];
        accBias.x = State[numStateAccBiasX];
        accBias.y = State[numStateAccBiasY];
        accBias.z = State[numStateAccBiasZ];
        gyoBias.x = State[numStateGyoBiasX];
        gyoBias.y = State[numStateGyoBiasY];
        gyoBias.z = State[numStateGyoBiasZ];
        quat.w = State[numStateQ0];
        quat.x = State[numStateQ1];
        quat.y = State[numStateQ2];
        quat.z = State[numStateQ3];
    }
    private Matrix4x4 quatCovarToEulerCovar(Matrix4x4 quatCov, Quaternion q)
    {
        Matrix4x4 Jacobi;
        
        float q1 = q.w;
        float q2 = q.x;
        float q3 = q.y;
        float q4 = q.z;
        Jacobi.m00 = -(q3 + q2) / ((q3 + q2) * (q3 + q2) + (q4 + q1) * (q4 + q1)) + (q3 - q2) / ((q3 - q2) * (q3 - q2) + (q4 - q1) * (q4 - q1));
        Jacobi.m01 = (q4 + q1) / ((q3 + q2) * (q3 + q2) + (q4 + q1) * (q4 + q1)) - (q4 - q1) / ((q3 - q2) * (q3 - q2) + (q4 - q1) * (q4 - q1));
        Jacobi.m02 = (q4 + q1) / ((q3 + q2) * (q3 + q2) + (q4 + q1) * (q4 + q1)) + (q4 - q1) / ((q3 - q2) * (q3 - q2) + (q4 - q1) * (q4 - q1));
        Jacobi.m03 = -(q3 + q2) / ((q3 + q2) * (q3 + q2) + (q4 + q1) * (q4 + q1)) - (q3 - q2) / ((q3 - q2) * (q3 - q2) + (q4 - q1) * (q4 - q1));
        Jacobi.m10 = 2 * q4 / Mathf.Sqrt(1.0f - 4 * (q2 * q3 + q1 * q4) * (q2 * q3 + q1 * q4));
        Jacobi.m11 = 2 * q3 / Mathf.Sqrt(1.0f - 4 * (q2 * q3 + q1 * q4) * (q2 * q3 + q1 * q4));
        Jacobi.m12 = 2 * q2 / Mathf.Sqrt(1.0f - 4 * (q2 * q3 + q1 * q4) * (q2 * q3 + q1 * q4));
        Jacobi.m13 = 2 * q1 / Mathf.Sqrt(1.0f - 4 * (q2 * q3 + q1 * q4) * (q2 * q3 + q1 * q4));
        Jacobi.m20 = -(q3 + q2) / ((q3 + q2) * (q3 + q2) + (q4 + q1) * (q4 + q1)) - (q3 - q2) / ((q3 - q2) * (q3 - q2) + (q4 - q1) * (q4 - q1));
        Jacobi.m21 = (q4 + q1) / ((q3 + q2) * (q3 + q2) + (q4 + q1) * (q4 + q1)) + (q4 - q1) / ((q3 - q2) * (q3 - q2) + (q4 - q1) * (q4 - q1));
        Jacobi.m22 = (q4 + q1) / ((q3 + q2) * (q3 + q2) + (q4 + q1) * (q4 + q1)) - (q4 - q1) / ((q3 - q2) * (q3 - q2) + (q4 - q1) * (q4 - q1));
        Jacobi.m23 = -(q3 + q2) / ((q3 + q2) * (q3 + q2) + (q4 + q1) * (q4 + q1)) + (q3 - q2) / ((q3 - q2) * (q3 - q2) + (q4 - q1) * (q4 - q1));
        Jacobi.m30 = 0;
        Jacobi.m31 = 0;
        Jacobi.m32 = 0;
        Jacobi.m33 = 0;
        return Jacobi*quatCov*Jacobi.transpose;
    }
    private void fromUKFtoDisp(IntPtr ukf, out Vector3 pos, out Vector3 vel, out Vector3 accBias, out Vector3 gyoBias, Quaternion quat, out Vector3 euler)
    {
        float[] covar = new float[numStates * numStates];
        IntPtr pState = KF_API.getPtrCovarMatrix(ukf);
        Marshal.Copy(pState, covar, 0, numStates* numStates);
        float mult = 100;
        pos.x = covar[numStatePosX + numStates* numStatePosX]* mult;
        pos.y = covar[numStatePosY + numStates * numStatePosY] * mult;
        pos.z = covar[numStatePosZ + numStates * numStatePosZ] * mult;
        vel.x = covar[numStateVelX + numStates * numStateVelX] * mult;
        vel.y = covar[numStateVelY + numStates * numStateVelY] * mult;
        vel.z = covar[numStateVelZ + numStates * numStateVelZ] * mult;
        accBias.x = covar[numStateAccBiasX + numStates * numStateAccBiasX] * mult;
        accBias.y = covar[numStateAccBiasY + numStates * numStateAccBiasY] * mult;
        accBias.z = covar[numStateAccBiasZ + numStates * numStateAccBiasZ] * mult;
        gyoBias.x = covar[numStateGyoBiasX + numStates * numStateGyoBiasX] * mult;
        gyoBias.y = covar[numStateGyoBiasY + numStates * numStateGyoBiasY] * mult;
        gyoBias.z = covar[numStateGyoBiasZ + numStates * numStateGyoBiasZ] * mult;
        Matrix4x4 quatCov;
        quatCov.m00 = covar[numStateQ0 + numStates * numStateQ0 + 0] * mult;
        quatCov.m01 = covar[numStateQ0 + numStates * numStateQ0 + 1] * mult;
        quatCov.m02 = covar[numStateQ0 + numStates * numStateQ0 + 2] * mult;
        quatCov.m03 = covar[numStateQ0 + numStates * numStateQ0 + 3] * mult;

        quatCov.m10 = covar[numStateQ1 + numStates * numStateQ1 - 1] * mult;
        quatCov.m11 = covar[numStateQ1 + numStates * numStateQ1 - 0] * mult;
        quatCov.m12 = covar[numStateQ1 + numStates * numStateQ1 + 1] * mult;
        quatCov.m13 = covar[numStateQ1 + numStates * numStateQ1 + 2] * mult;

        quatCov.m20 = covar[numStateQ2 + numStates * numStateQ2 - 2] * mult;
        quatCov.m21 = covar[numStateQ2 + numStates * numStateQ2 - 1] * mult;
        quatCov.m22 = covar[numStateQ2 + numStates * numStateQ2 + 0] * mult;
        quatCov.m23 = covar[numStateQ2 + numStates * numStateQ2 + 1] * mult;

        quatCov.m30 = covar[numStateQ3 + numStates * numStateQ3 - 3] * mult;
        quatCov.m31 = covar[numStateQ3 + numStates * numStateQ3 - 2] * mult;
        quatCov.m32 = covar[numStateQ3 + numStates * numStateQ3 - 1] * mult;
        quatCov.m33 = covar[numStateQ3 + numStates * numStateQ3 + 0] * mult;

        var eulerCovar = quatCovarToEulerCovar(quatCov, quat);
        euler.x = eulerCovar.m00*180.0f/Mathf.PI * mult;
        euler.y = eulerCovar.m22 * 180.0f / Mathf.PI * mult;
        euler.z = eulerCovar.m11 * 180.0f / Mathf.PI * mult;
    }
    private void FixedUpdate()
    {
        KF_API.vec3 accIn, gyoIn, zDir;
        accIn.x = imu.Acc.x;
        accIn.y = imu.Acc.y;
        accIn.z = imu.Acc.z;

        gyoIn.x = imu.Gyo.x;
        gyoIn.y = imu.Gyo.y;
        gyoIn.z = imu.Gyo.z;

        //zDir.x = -0.707f;
        //zDir.y = 0;
        //zDir.z = 0.707f;

        zDir.x = -1.0f;
        zDir.y = 0;
        zDir.z = 0.0f;

        //zDir.x = 0.0f;
        //zDir.y = 0;
        //zDir.z = 1.0f;

        //forecasting
        KF_API.timeUpdate(ukf, accIn, gyoIn, zDir, Time.deltaTime);

        if (flagGnssReady)
        {
            flagGnssReady = false;
            KF_API.vec3 pos, posLoc;
            pos.x = gnssData.x;
            pos.y = gnssData.y;
            pos.z = gnssData.z;
            posLoc.x = localPosGPSOfIMU.x;
            posLoc.y = localPosGPSOfIMU.y;
            posLoc.z = localPosGPSOfIMU.z;
            //GNSS correction
            KF_API.gpsPosUpdate(ukf, pos, posLoc, SDNoiseGNSS);
        }

        //pseudo measurment while standing
        if (Time.timeSinceLevelLoad > 1.5 && Time.timeSinceLevelLoad < 3)
        {
            KF_API.zeroRate(ukf, gyoIn);
            KF_API.zeroVel(ukf);
        }
    
        //translation 
        fromUKFtoVectors(ukf, out pos, out vel, out accBias, out gyoBias, out quat);
        fromUKFtoDisp(ukf, out sigma_pos, out sigma_vel, out sigma_accBias, out sigma_gyoBias, quat, out sigma_euler);
        //visualization 3D object
        if (estimateMarker)
        {
            estimateMarker.transform.position = pos;
            estimateMarker.transform.rotation = quat;
        }
#if DEBUG_TEST
        string data = "\r\n"+ Time.time.ToString().Replace(',', '.') + delimeter + 
            imu.Pos.x.ToString().Replace(',', '.') + delimeter + pos.x.ToString().Replace(',', '.') + delimeter + sigma_pos.x.ToString().Replace(',', '.') + delimeter +
            imu.Pos.y.ToString().Replace(',', '.') + delimeter + pos.y.ToString().Replace(',', '.') + delimeter + sigma_pos.y.ToString().Replace(',', '.') + delimeter +
            imu.Pos.z.ToString().Replace(',', '.') + delimeter + pos.z.ToString().Replace(',', '.') + delimeter + sigma_pos.z.ToString().Replace(',', '.') + delimeter +

            imu.Vel.x.ToString().Replace(',', '.') + delimeter + vel.x.ToString().Replace(',', '.') + delimeter + sigma_vel.x.ToString().Replace(',', '.') + delimeter +
            imu.Vel.y.ToString().Replace(',', '.') + delimeter + vel.y.ToString().Replace(',', '.') + delimeter + sigma_vel.y.ToString().Replace(',', '.') + delimeter +
            imu.Vel.z.ToString().Replace(',', '.') + delimeter + vel.z.ToString().Replace(',', '.') + delimeter + sigma_vel.z.ToString().Replace(',', '.') + delimeter +

            imu.biasAcc.x.ToString().Replace(',', '.') + delimeter + accBias.x.ToString().Replace(',', '.') + delimeter + sigma_accBias.x.ToString().Replace(',', '.') + delimeter +
            imu.biasAcc.y.ToString().Replace(',', '.') + delimeter + accBias.y.ToString().Replace(',', '.') + delimeter + sigma_accBias.y.ToString().Replace(',', '.') + delimeter +
            imu.biasAcc.z.ToString().Replace(',', '.') + delimeter + accBias.z.ToString().Replace(',', '.') + delimeter + sigma_accBias.z.ToString().Replace(',', '.') + delimeter +

            imu.biasGyo.x.ToString().Replace(',', '.') + delimeter + gyoBias.x.ToString().Replace(',', '.') + delimeter + sigma_gyoBias.x.ToString().Replace(',', '.') + delimeter +
            imu.biasGyo.y.ToString().Replace(',', '.') + delimeter + gyoBias.y.ToString().Replace(',', '.') + delimeter + sigma_gyoBias.y.ToString().Replace(',', '.') + delimeter +
            imu.biasGyo.z.ToString().Replace(',', '.') + delimeter + gyoBias.z.ToString().Replace(',', '.') + delimeter + sigma_gyoBias.z.ToString().Replace(',', '.') + delimeter +

            imu.Rot.eulerAngles.x.ToString().Replace(',', '.') + delimeter + quat.eulerAngles.x.ToString().Replace(',', '.') + delimeter + sigma_euler.x.ToString().Replace(',', '.') + delimeter +
            imu.Rot.eulerAngles.y.ToString().Replace(',', '.') + delimeter + quat.eulerAngles.y.ToString().Replace(',', '.') + delimeter + sigma_euler.y.ToString().Replace(',', '.') + delimeter +
            imu.Rot.eulerAngles.z.ToString().Replace(',', '.') + delimeter + quat.eulerAngles.z.ToString().Replace(',', '.') + delimeter + sigma_euler.z.ToString().Replace(',', '.');
        writer.Write(data);
#endif



    }

    public class KF_API
    {
#if !DEBUG_TEST
        const string libName = "__Internal";
#else
        const string libName = "kalman_lib";
#endif

        [StructLayout(LayoutKind.Sequential)]
        public struct vec3
        {
            internal float x, y, z;
        }
        [DllImport(libName)]
        public static extern IntPtr initKF(float[] StateVectorInit);

        [DllImport(libName)]
        public static extern void destroy(IntPtr kf);

        [DllImport(libName)]
        public static extern IntPtr getPtrState(IntPtr kf);

        [DllImport(libName)]
        public static extern IntPtr getPtrCovarMatrix(IntPtr kf);

        [DllImport(libName)]
        public static extern void zeroVel(IntPtr ukf);

        [DllImport(libName)]
        public static extern void zeroRate(IntPtr ukf, vec3 rate);

        [DllImport(libName)]
        public static extern void gpsPosUpdate(IntPtr kf, vec3 pos, vec3 posLoc, float noise);

        [DllImport(libName)]
        public static extern void timeUpdate(IntPtr kf, vec3 acc, vec3 gyox, vec3 dir, float dt);

        [DllImport(libName)]
        public static extern void setInitVariance(IntPtr kf, float initDispVel, float initDispPos, float initDispAccBias, float initDispGyoBias, float initDispQuat);

        [DllImport(libName)]
        public static extern void setProcessVariance(IntPtr kf, float processDispVel, float processDispRate, float processDispAccBias, float processDispGyoBias);

    }

}
