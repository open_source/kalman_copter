using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

public class GNSSModel : MonoBehaviour
{
    public Vector3 data;
    [Tooltip("active additive white noise")]
    public bool activeNoise;
    [Tooltip("active random walk")]
    public bool activeBias;

    [Tooltip("frequency of data")]
    public float freq;
    [Tooltip("LPF frequency cutoff random walk")]
    public float freqCut;
    [Tooltip("coef for HPF random walk, must be lower than 1")]
    public float KStray;
    [Tooltip("standard deviation random walk")]
    public float speedSD;

    [Tooltip("standard deviation additive white noise")]
    public float SDWnoise;

    public delegate void dataReady(Vector3 data);
    public event dataReady OnDataReady = delegate { };

    private Rigidbody body;
    static private System.Random rand1;

    private int delayfreq;
    private int cntfreq;

    private Vector3 biassum;
    private Vector3 lpf;


    //methods for generation of noise
    private float normrand()
    {
        float x = 0.0f;
        for (short i = 0; i < 12; i++)
            x += (float)rand1.Next(0, 32767) / 32767.0f;
        return x - 6.0f;
    }
    private float gausNoise(float SD)
    {
        float val = normrand() * SD;
        return (Mathf.Abs(val) > 3.0f * SD) ? 3.0f * SD : val;
    }


    // Start is called before the first frame update
    void Start()
    {
        rand1 = new System.Random();
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetFreq(string freqStr)
    {
        freq = float.Parse(freqStr.Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator), CultureInfo.InvariantCulture);
    }

    private void FixedUpdate()
    {
        delayfreq = (int)(1f / ((float)Time.deltaTime * freq));
        if (delayfreq == 0)
            delayfreq = 1;

        float kLPF = (freqCut) / (1.0f + freqCut / freq);
        
        cntfreq = (cntfreq + 1) % delayfreq;

        
        if (cntfreq % delayfreq == 0)
        {
            data = body.position;


            //random walk
            if (activeBias)
            {              
                biassum.x = biassum.x * KStray + gausNoise(speedSD);
                biassum.z = biassum.z * KStray + gausNoise(speedSD);
                biassum.y = biassum.y * KStray + gausNoise(speedSD);

                lpf = (1.0f - kLPF) * lpf + kLPF * biassum;

                data += lpf;
            }

            
            if (activeNoise)
            {               
                data.x += gausNoise(SDWnoise);
                data.z += gausNoise(SDWnoise);
                data.y += gausNoise(SDWnoise);
            }

            var events = OnDataReady.GetInvocationList();
            for (int i = 1; i < events.Length; i++)
            {
                var callbackRX = events[i] as dataReady;
                callbackRX(data);
            }

        }

    }
}
