﻿#include "nav_kf.h"


extern "C" nav_kf *initKF(float *StateVectorInit, int history, int horizon)
{
	nav_kf *ukf = new nav_kf(StateVectorInit);
	return ukf;
}
extern "C" void destroy(nav_kf *ukf) {
	delete(ukf);
}
extern "C" float *getPtrState(nav_kf *ukf) {
	return ukf->GetPtrState();
}
extern "C" float *getPtrCovarMatrix(nav_kf *ukf) {
	return ukf->GetPtrCovarMatrix();
}
extern "C" void zeroRate(nav_kf *ukf, vector_t rate)
{
	ukf->ZeroRate(rate);
}
extern "C" void zeroVel(nav_kf *ukf)
{
	ukf->ZeroVel();
}
extern "C" void gpsPosUpdate(nav_kf *ukf, vector_t pos, vector_t posloc, float noise)
{
	ukf->GpsPosUpdate(pos, posloc, noise);
}
extern "C" void timeUpdate(nav_kf *ukf, vector_t acc, vector_t gyo, vector_t dir, float dt)
{
	ukf->InertialUpdate(acc, gyo, dir, dt);
}
extern "C" void setInitVariance(nav_kf * ukf, float initDispVel, float initDispPos, float initDispAccBias, float initDispGyoBias, float initDispQuat)
{
	ukf->SetInitVariance(initDispVel, initDispPos, initDispAccBias, initDispGyoBias, initDispQuat);
}
extern "C" void setProcessVariance(nav_kf *  ukf, float processDispVel, float processDispRate, float processDispAccBias, float processDispGyoBias)
{
	ukf->SetProcVariance(processDispVel, processDispRate, processDispAccBias, processDispGyoBias);
}
